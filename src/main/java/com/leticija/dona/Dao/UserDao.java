package com.leticija.dona.Dao;

import com.leticija.dona.HibernateUtil;
import com.leticija.dona.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class UserDao extends BaseDao<User> {

    private Transaction transaction;

    public void begin() {
        try {
            // start a transaction
            transaction=session.beginTransaction();
            // save the student object
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void commit () {
        try {
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void saveUser(User user) {
            boolean keep = false;
            if (transaction != null && transaction.isActive())
                keep = true;
            if (transaction==null)
                begin();
            session.save(user);
            if (!keep)
                commit();
    }

    public List<User> getUsers() {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from "+User.class.getName(), User.class).list();
        }

    }
}