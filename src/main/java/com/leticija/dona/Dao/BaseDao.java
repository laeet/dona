package com.leticija.dona.Dao;

import com.leticija.dona.HibernateUtil;
import com.leticija.dona.Server;
import io.javalin.plugin.json.JavalinJackson;
import org.hibernate.Session;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public abstract class BaseDao<Entity> {

    private static final Logger log = LoggerFactory.getLogger(Server.class);
    Session session;
    Class<Entity> typeParameterClass;
    private String[] columnNames;

    BaseDao () {
        session = HibernateUtil.getSessionFactory().openSession();
        typeParameterClass = (Class<Entity>)
                ((ParameterizedType)getClass().getGenericSuperclass())
                        .getActualTypeArguments()[0];
    }

    public void printFewRows(int numberOfRows) {

    }

    //TODO: napisati generičniju funkciju koja prima objekt i provjerava jel on postoji u tablici
    public boolean doesExist() {

        Query query = HibernateUtil.newSession().createQuery("FROM "+typeParameterClass.getName()+" WHERE username= :username AND password= :password", typeParameterClass);
        //query.setParameter("username", user.getUsername());
        //query.setParameter("password", user.getPassword());

        List queryList = query.getResultList();

        if (queryList.isEmpty()) {

            System.out.println("NO, DOESN'T EXIST.");
            return false;
        } else {

            System.out.println("YES, EXISTS.");
            return true;
        }
    }

    public String[] getColumnNames() {

        ClassMetadata hibernateMetadata = HibernateUtil.getSessionFactory().getClassMetadata(typeParameterClass);

        if (hibernateMetadata == null) {return null;}

        else if (hibernateMetadata instanceof AbstractEntityPersister)
        {
            AbstractEntityPersister persister = (AbstractEntityPersister) hibernateMetadata;
            return persister.getKeyColumnNames();
        }
        return null;

    }

    // Generic method impl for finding entities in DB
    public List<Entity> findBy(Map<String, Object> properties) throws IOException {
        log.info(JavalinJackson.getObjectMapper().writeValueAsString(properties));
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Entity> cq = cb.createQuery(typeParameterClass);
        Root<Entity> root = cq.from(typeParameterClass);
        Predicate where = cb.and();
        if (properties != null) {
            for(Map.Entry<String, Object> entry : properties.entrySet()) {
                where = cb.and(where,cb.equal(root.get(entry.getKey()), entry.getValue()));
            }
        }
        Query<Entity> query = session.createQuery(cq.where(where));
        return query.getResultList();
    }

}
