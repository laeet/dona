package com.leticija.dona;

import com.leticija.dona.model.User;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;


public class Database {
    private static String numberFromRS;

    /**
     * @return databaseConnection
     * @throws Exception
     */

    public static Session newSession() {
        //opening session
        return HibernateUtil.getSessionFactory().openSession();

    }

    public static boolean doesUserExists(User user, String tableName) {

        Query query = newSession().createQuery("FROM User WHERE username= :username AND password= :password", User.class);
        query.setParameter("username", user.getUsername());
        query.setParameter("password", user.getPassword());

        List queryList = query.getResultList();

        if (queryList.isEmpty()) {

            System.out.println("NO, DOESN'T EXIST.");
            return false;
        }
        else {

            System.out.println("YES, EXISTS.");
            return true;
        }
    }

    public static Connection getConnection () throws Exception {

        //String DB_URL = "jdbc:h2:/media/leticija/88fe59cb-92ec-4a23-8e33-888bbe6ff16b/bugy_backup/final_base";
        String DB_URL = EnvironmentVariables.getInstance().DATABASE_PROTO + EnvironmentVariables.getInstance().DATABASE_PATH;
        Class.forName(EnvironmentVariables.getInstance().JDBC_DRIVER);

        return(DriverManager.getConnection(DB_URL));
    }

    //stare funkcije
    public static String getUserId (String username, String password) {

        System.out.println("GETTING USER ID FROM DATABASE...");
        String userIdToReturn = "";
        try (Connection conn = getConnection()){
            PreparedStatement getUserId = conn.prepareStatement ("SELECT * FROM users WHERE `username`= ? AND `password`= ?",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            getUserId.setString(1,username);
            getUserId.setString(2,password);

            ResultSet rs = getUserId.executeQuery();
            rs.next();
            userIdToReturn = rs.getString("id");

        }catch (Exception e) {
            e.printStackTrace();
        }

        return userIdToReturn;
    }

    public static String getUserEmail (String userId) {

        String userEmailToReturn = "";

        try (Connection conn = getConnection()){

            PreparedStatement userEmail = conn.prepareStatement ("SELECT * FROM users WHERE id = ?",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            userEmail.setString(1,userId);
            ResultSet rs = userEmail.executeQuery();
            rs.next();
            userEmailToReturn = rs.getString("email");

        }catch (Exception e) {
            e.printStackTrace();
        }

        return userEmailToReturn;

    }

    public static String exists (String username,String password) {

        System.out.println("Connecting to user database, checking if exists...");
        try (Connection conn = getConnection()){
            PreparedStatement count = conn.prepareStatement ("SELECT COUNT (*) FROM users WHERE `username`= ? AND `password`= ? LIMIT 1",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            count.setString (1,username);count.setString(2,password);
            ResultSet res = count.executeQuery();
            res.next();
            //test
            numberFromRS = res.getString("count(*)");//kasnije se provjerava, ako je 1, onda se nemre registrirati
            count.close();

        }catch (Exception e) {
            e.printStackTrace();
        }
        if(numberFromRS.equals("0")) {
            return("false");
        }
        return("true");
    }

    public static void printTable (String tableName) {
        try (Connection conn = getConnection();
             PreparedStatement getTable = conn.prepareStatement("SELECT * FROM `"+tableName+"`", ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE)){
            ResultSet rs = getTable.executeQuery();
            System.out.println("tablica "+tableName);
            PrettyPrinter.printQuery(rs);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printFewRows(String tableName, int numberOfRows) {
        try (Connection conn = getConnection();
             PreparedStatement getTable = conn.prepareStatement("SELECT * FROM `"+tableName+"` WHERE ID = ? LIMIT 1", ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE)){
            System.out.println("tablica " + tableName);
            for (int i = 1; i <= numberOfRows; i++) {
                getTable.setString(1, String.valueOf(i));
                ResultSet rs = getTable.executeQuery();
                System.out.println("redak: " + i);
                PrettyPrinter.printQuery(rs);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printFewColumnValues(String tableName, String columnName, int numberOfColumns) {

        try (Connection conn = getConnection();
             PreparedStatement getTable = conn.prepareStatement("SELECT * FROM `"+tableName+"`", ResultSet.TYPE_SCROLL_INSENSITIVE)){
            System.out.println("tablica: " + tableName);
            ResultSet rs = getTable.executeQuery();
            String value;
            for (int i = 1; i <= numberOfColumns; i++) {
                rs.next();
                value = rs.getString(columnName);
                System.out.println("redak: " + i);
                System.out.println("link: "+rs.getString("URL"));
                System.out.println("vrijednost: "+value);
                System.out.println("---------------------");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteTable(String tableName) {
        System.out.println ("DELETING TABLE");
        try (Connection conn = getConnection()) {
            PreparedStatement delTable = conn.prepareStatement("DROP TABLE `"+tableName+"`");
            delTable.executeUpdate();
            delTable.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getNumberOfRows (String tableName) {
        try (Connection conn = Database.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT COUNT(*) FROM "+tableName,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = ps.executeQuery();
            // get the number of rows from the result set
            rs.next();
            int rowCount = rs.getInt(1);
            return rowCount;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void deleteUser (String ID) {

        try (Connection conn = Database.getConnection()) {

            System.out.println("TABLE BEFORE DELETION:");
            Database.printTable("properties");

            PreparedStatement deleteFromProperties = conn.prepareStatement("DELETE FROM `properties` WHERE `user_id`=?");
            deleteFromProperties.setString(1,ID);
            deleteFromProperties.executeUpdate();
            deleteFromProperties.close();

            System.out.println("TABLE AFTER DELETION:");
            Database.printTable("properties");
            printTable("users");

            PreparedStatement deleteUser = conn.prepareStatement("DELETE FROM users WHERE id = ?");
            deleteUser.setString(1,ID);
            deleteUser.executeUpdate();
            deleteUser.close();
            Database.printTable("users");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updatePublicDescription (String bugId, String pubDesc) {

        try {
            Connection conn = Database.getConnection();

            PreparedStatement doesInsectExistsInPubDescTable = conn.prepareStatement("SELECT * FROM public_description WHERE insect_id = ?");
            doesInsectExistsInPubDescTable.setString(1, bugId);
            ResultSet resultSet = doesInsectExistsInPubDescTable.executeQuery();

            if (resultSet.next()) {
                System.out.println("YES, INSECT ALREADY HAS PUBLIC DESC!");
                PreparedStatement updatePublicDesc = conn.prepareStatement("UPDATE public_description SET pub_desc = ? WHERE insect_id = ?");
                updatePublicDesc.setString(1, pubDesc);
                updatePublicDesc.setString(2, bugId);

                updatePublicDesc.executeUpdate();
                updatePublicDesc.close();

            } else {
                System.out.println("NO, INSECT DOES NOT EXIST.");
                PreparedStatement addInsectToPubDesc = conn.prepareStatement("INSERT INTO public_description (insect_id,pub_desc) VALUES (?,?)");
                addInsectToPubDesc.setString(1, bugId);
                addInsectToPubDesc.setString(2, pubDesc);

                addInsectToPubDesc.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
