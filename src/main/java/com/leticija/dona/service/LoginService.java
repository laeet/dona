package com.leticija.dona.service;

import com.leticija.dona.Dao.UserDao;
import com.leticija.dona.model.User;
import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJackson;
import lombok.Data;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Data
public class LoginService {

    Context context;

    public LoginService (Context context) {
        this.context = context;
    }

    public User unpackFormData () {

        //prvotno je formParam mapa ključ:lista
        //mapiranje formParam mape u novu mapu <String,String>
        Map<String, Object> newMap = context.formParamMap().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get(0)));
        //konvertiranje mape <String,String> u objekt User pomoću Jacksona
        User user = JavalinJackson.getObjectMapper().convertValue(newMap,User.class);

        System.out.println("USER USERNAME: "+user.getUsername());

        return user;
    }

    public boolean validate (User user) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        if(violations.isEmpty()) {
            return true;
        }
        else {
            for (ConstraintViolation<User> violation : violations) {
                System.out.println(violation.getMessage());
            }
            return false;
        }

    }

    public boolean authenticate () throws IOException {

        User user = unpackFormData();

        if (validate(user)) {
            //search table for user
            System.out.println("IM IN AFTER VALIDATION!!!");
            Map<String,Object> properties = new HashMap<String, Object>() {{
                put("username",user.getUsername());
                put("password",user.getPassword());
            }};
            UserDao userDao = new UserDao();
            if (!userDao.findBy(properties).isEmpty()) {
                System.out.println("CHECK RESULT: "+userDao.findBy(properties));
                context.redirect("/html/home.html");
                return true;
            } else {
                context.redirect("/html/login.html");
            }
        }
        else {
            //TODO: not validated!!!
            //do something in this case
            //show message on login page
            System.out.println("invalid credentials!!!!!");
            return false;
        }
        return false;
    }

    public void login (User user) {

        //TODO: things that should be done while logging in

    }

}
