package com.leticija.dona.model;

import lombok.Data;

import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "User")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotNull(message = "Username cannot be null")
    @Column(name = "username")
    private String username;

    @NotNull(message = "Password cannot be null")
    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

}