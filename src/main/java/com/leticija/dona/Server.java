package com.leticija.dona;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.leticija.dona.Dao.UserDao;
import com.leticija.dona.model.User;
import com.leticija.dona.service.LoginService;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.rendering.JavalinRenderer;
import io.javalin.plugin.rendering.template.JavalinPebble;
import org.eclipse.jetty.server.session.DefaultSessionCache;
import org.eclipse.jetty.server.session.FileSessionDataStore;
import org.eclipse.jetty.server.session.SessionCache;
import org.eclipse.jetty.server.session.SessionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Server {

	private static final Logger log = LoggerFactory.getLogger(Server.class);

	public static void main(String[] args) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

		// postavimo taj nas mapper da ga koristi javalin pa ga moremo koristiti bilo gde prek JavalinJackson.getObjectMapper()
		JavalinJackson.configure(mapper);
		JavalinRenderer.register(JavalinPebble.INSTANCE,".peb");
		JavalinRenderer.register(JavalinPebble.INSTANCE::render);

		Javalin app = Javalin.create(config -> {
			config.enableDevLogging();
			config.addStaticFiles("/static", Location.CLASSPATH);
			config.sessionHandler(() -> new SessionHandler(){
				{
					FileSessionDataStore fileSessionDataStore = new FileSessionDataStore();
					File storeDir = new File( "javalin-session-store");
					storeDir.mkdir();
					fileSessionDataStore.setStoreDir(storeDir);
					SessionCache cache = new DefaultSessionCache(this);
					cache.setSessionDataStore(fileSessionDataStore);
					setSessionCache(cache);
					setMaxInactiveInterval(1);
				}
			});
		}).start(EnvironmentVariables.getInstance().PORT); // na portu 7000
		Runtime.getRuntime().addShutdownHook(new Thread(app::stop));

		log.info("~ Javalin server started ~");

		// probni objekt User
		User pia = new User();
		UserDao userDao = new UserDao();

		//hashmap for checking if user exists in table
		Map<String,Object> toChecc = new HashMap<String,Object>(){{
			put("username","bernardt");
		}};
		//printing hashmap returned by function findBy()
		System.out.println("CHECKED: "+userDao.findBy(toChecc));
		//printing all users from table user
		System.out.println("HERE ARE ALL USERS: ");
		userDao.getUsers().forEach(s -> {
					try {
						System.out.println(mapper.writeValueAsString(s));
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

		//endpoints
		app.post("/submitCredentials",ctx -> {
			System.out.println("THOSE ARE CREDENTIALS TO LOG IN:");
			System.out.println(mapper.writeValueAsString(ctx.formParamMap()));
		});

		app.get("/index", ctx -> {
			Map<String, Object> context = new HashMap<>();
			context.put("a", "b");
			ctx.render("index.peb", context);
		});

		app.post("/login",ctx -> {
			System.out.println("USER WANTS TO LOGIN!: "+ctx.formParamMap());

			LoginService loginService= new LoginService(ctx);
			loginService.authenticate();
			//ctx.redirect("/html/home.html");
		});

		app.post("/register",ctx -> {
			log.info("USER WANTS TO REGISTER: "+ctx.formParamMap());
			ctx.redirect("/html/home.html");
		});

		app.get("/", ctx -> {

			ctx.json(pia);
		});
		app.get("/getText", ctx -> {
			ctx.json(pia);

		});
	}
}