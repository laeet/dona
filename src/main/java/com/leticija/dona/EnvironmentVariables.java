package com.leticija.dona;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class EnvironmentVariables {

    public String SQL_QUERIES_PATH;
    public String DATABASE_PROTO;
    public String JDBC_DRIVER;
    public String DATABASE_PATH;
    public int PORT;

    private static final ObjectMapper mapper = new ObjectMapper();

    private static EnvironmentVariables instance;

    static {
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {
            instance = mapper.readValue(EnvironmentVariables.class.getResourceAsStream("/config.json"), EnvironmentVariables.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(instance));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private EnvironmentVariables(){}

    public static EnvironmentVariables getInstance() {
        return instance;
    }

}
